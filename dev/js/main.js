$(document).ready(function() {

    // switcher
    $('.switcher').click(function() {
        $(this).find('.switcher__dropdown').slideToggle('1');

    });

    $('.switcher__dropdown')
        // .mousedown(function(){$('.switcher__dropdown').show();})
        .mouseleave(function() {
            $('.switcher__dropdown').hide();
        });

    //hamburger

    $('#hamburger').click(function() {
        $(this).toggleClass('open');
        $('#nav').slideToggle('slow');

    });

    $('.category-list-title').click(function() {

        $(this).next('.category-list').slideToggle('slow');
        $(this).toggleClass('open');

    });

    // Dropdown Box Toggle

    $('.jsDropdownBoxToggle').click(function(e) {

        $(this).next('.dropdown-box__content').slideToggle('slow');
        $(this).toggleClass('open');

    });


    //sliders

    $('.banner-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" width="7" height="12" viewBox="0 0 7 12"><path  d="M.72 11.88l-.6-.6a.38.38 0 0 1 0-.55L4.85 6 .12 1.27a.38.38 0 0 1 0-.55l.6-.6A.4.4 0 0 1 1 0c.1 0 .2.04.28.12l5.6 5.6a.38.38 0 0 1 0 .55l-5.6 5.6A.4.4 0 0 1 1 12a.4.4 0 0 1-.28-.12z"/></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><span><svg xmlns="http://www.w3.org/2000/svg" width="7" height="12" viewBox="0 0 7 12"><path d="M6.28 11.88l.6-.6a.38.38 0 0 0 0-.55L2.15 6l4.73-4.73a.38.38 0 0 0 0-.55l-.6-.6a.38.38 0 0 0-.55 0l-5.6 5.6a.38.38 0 0 0 0 .56l5.6 5.6a.38.38 0 0 0 .55 0z"/></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        // autoplay:true,
    });
    $('.product__img-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" width="7" height="12" viewBox="0 0 7 12"><path  d="M.72 11.88l-.6-.6a.38.38 0 0 1 0-.55L4.85 6 .12 1.27a.38.38 0 0 1 0-.55l.6-.6A.4.4 0 0 1 1 0c.1 0 .2.04.28.12l5.6 5.6a.38.38 0 0 1 0 .55l-5.6 5.6A.4.4 0 0 1 1 12a.4.4 0 0 1-.28-.12z"/></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><span><svg xmlns="http://www.w3.org/2000/svg" width="7" height="12" viewBox="0 0 7 12"><path d="M6.28 11.88l.6-.6a.38.38 0 0 0 0-.55L2.15 6l4.73-4.73a.38.38 0 0 0 0-.55l-.6-.6a.38.38 0 0 0-.55 0l-5.6 5.6a.38.38 0 0 0 0 .56l5.6 5.6a.38.38 0 0 0 .55 0z"/></svg></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,

        // autoplay:true,
    });


    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,


    });

    $('.product-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-slider-nav'
    });

    $('.product-slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-slider',
        focusOnSelect: true,
        vertical: true,
        arrows: false,
        responsive: [{
                breakpoint: 485,
                settings: {
                    vertical: false

                }
            }
        ]
    });



    // phone mask
        $(function() {
            $('[name="tel"]').mask("+0 000 000 00 00", {
                clearIfNotMatch: true,
                placeholder: "+ 0 000 00 00 00"
            });
            $('[name="tel"]').focus(function(e) {
                if ($('[name="tel"]').val().length == 0) {
                    $(this).val('+');
                }
            })
        });


    //equal hieght


    //form styler
    (function($) {
        $(function() {
            $('input, select').styler({

            });
        });
    })(jQuery);

    //BasketRemoveItem
    $('.jsBasketRemoveItem').click(function(e) {
        e.preventDefault();
        var id = $(this).closest('.tr').attr('data-elementId'),
            relatedElements = $('.main-basket .tr[data-elementId=' + id + ']');
        relatedElements.remove();
        footerPosition();
    });

    $('.jsInBasketRemoveItem').click(function(e) {
        e.preventDefault();
        $(this).parent('.inbasket__item').remove();

    });






});
;
(function($, window, document, undefined) {
    'use strict';

    var $list = $('.eq-wrap'),
        $items = $list.find('.eq-item'),
        setHeights = function() {
            $items.css('height', 'auto');

            var perRow = Math.floor($list.width() / $items.width());
            if (perRow == null || perRow < 2) return true;

            for (var i = 0, j = $items.length; i < j; i += perRow) {
                var maxHeight = 0,
                    $row = $items.slice(i, i + perRow);

                $row.each(function() {
                    var itemHeight = parseInt($(this).outerHeight());
                    if (itemHeight > maxHeight) maxHeight = itemHeight;
                });
                $row.css('height', maxHeight);
            }
        };

    setHeights();
    $(window).on('resize', setHeights);
    $list.find('img').on('load', setHeights);
})(jQuery, window, document);
