var gulp=require('gulp'),
	sass=require('gulp-sass'),
	jade=require('gulp-jade'),
	browserSync=require('browser-sync'),
	concat=require('gulp-concat'),
	uglify=require('gulp-uglify'),
	cssnano=require('gulp-cssnano'),
	rename=require('gulp-rename'),
	del=require('del'),
	imagemin=require('gulp-imagemin'),
    cache=require('gulp-cache'),
    pngquant=require('imagemin-pngquant'),
    autoprefixer=require('gulp-autoprefixer'),
    spritesmith = require('gulp.spritesmith'),
    mainBowerFiles = require('main-bower-files'),
    inject = require('gulp-inject'),
    runSequence = require('run-sequence'),
    series = require('stream-series'),
    svgToSss = require('gulp-svg-to-css');




// inject into jade

gulp.task('injectJS', function() {

    var vendorStreamJS = gulp.src(['./app/vendor/**/*.js', '!./app/vendor/**/jquery.js' ], {read: false});
    var   appStreamJS = gulp.src(['./app/js/**/*.js'], {read: false});
    var vendorjquerystreamJS = gulp.src('./app/vendor/**/jquery.js', {read: false});

    gulp.src('./app/*.jade')
        // .pipe(inject(gulp.src('./dev/vendor/**/jquery.js', {read: false}), {name: 'jquery'}, {relative: true})) 
        .pipe(inject(series(vendorjquerystreamJS), {name: 'jquery', relative: true}))
        .pipe(inject(series(vendorStreamJS, appStreamJS), {relative: true})) // This will always inject vendor files before app files
        .pipe(gulp.dest('./app'))
});



gulp.task('injectCSS', function() {
    var vendorStreamCSS = gulp.src(['./app/vendor/**/*.css'], {read: false});
    var appStreamCSS = gulp.src(['./app/css/**/*.css'], {read: false});

    gulp.src('./app/*.jade')
      .pipe(inject(series(vendorStreamCSS, appStreamCSS), {relative: true})) // This will always inject vendor files before app files
      .pipe(gulp.dest('./app'))
});


// inject into jade end


 //mainBowerFiles start

gulp.task('mainJS', function() {
    return gulp.src(mainBowerFiles('**/**/*.js', {
      "overrides": {
        "bootstrap": {
            "main": [
                "./dist/js/bootstrap.min.js"
                ]
        },
        "jquery.form-styler":{
            "main": [
        "./dist/js/jquery.formstyler.min.js",
        
    ]
        }
    }
}))
    .pipe(gulp.dest('app/vendor/js'))
});

gulp.task('mainCSS', function() {
    return gulp.src(mainBowerFiles('**/**/*.css', {
      "overrides": {
        "bootstrap": {
            "main": [
                "./dist/css/bootstrap.min.css"
                
                ]
        },
        "jquery.form-styler":{
            "main": [
        "./dist/css/jquery.formstyler.min.css",
        
    ]
        }
    }}))
    .pipe(gulp.dest('app/vendor/css'))
});

//mainBowerFiles  end

// jade to html srart
gulp.task('html', function() {
  var YOUR_LOCALS = {};
 
  gulp.src('./app/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty: true
    }))
    .pipe(gulp.dest('./app/'))
});
// jade to html end


// sass to css
gulp.task('sass', function(){
	return gulp.src('app/sass/*.scss')
		.pipe(sass())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(['last 15 version', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
	return gulp.src([
		
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
});

// jade to html end


// local host
gulp.task('browser-sync', function(){
	browserSync({
		server:{
			baseDir: 'app'
		},
		notify: true
	});
});
// local host end



// clear start
gulp.task('clean', function() {
    return del.sync('dev');
});

gulp.task('clear', function() {
    return cache.clearAll();
});
// clear end



// img compress
gulp.task('img', function() {
    return gulp.src('app/img/**/*') 
        .pipe(cache(imagemin({
            interlaced: false,
            progressive: false,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dev/img')); 
});
// img compress end



// sprite
gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('app/img/sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.css',
                padding: 5,
                imgPath: '../img/sprite.png',
                
            }));

    spriteData.img.pipe(gulp.dest('app/img/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('app/sass/')); // путь, куда сохраняем стили
});

// sprite end

// svg to css


 
gulp.task('svg', function(){
    gulp.src('app/img/**/*.svg')
    .pipe(svgToSss('_svg.scss'))
    .pipe(gulp.dest('app/sass/'));
    
});

// end svg to css

// inject


gulp.task('inject', function(done) {
    runSequence('mainJS', 'mainCSS',['injectJS', 'injectCSS'],

        done);

});



// default
gulp.task('watch', ['browser-sync', 'sass', 'scripts', 'html'], function(){
	gulp.watch('app/sass/*.scss', ['sass']);
	gulp.watch('app/**/*.jade', ['html']);
	gulp.watch('app/**/*.html', browserSync.reload);
	gulp.watch('app/**/**/*.js', browserSync.reload);
	gulp.watch('app/**/**/*.css', browserSync.reload);
});


// build app
gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function() {
     var vendor = gulp.src('app/vendor/**/*')
    .pipe(gulp.dest('dev/vendor'));

    var buildCss = gulp.src('app/css/**/*')
    .pipe(gulp.dest('dev/css'));

    var buildFonts = gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dev/fonts'));

    var buildJs = gulp.src('app/js/**/*')
    .pipe(gulp.dest('dev/js'));

    var buildHtml = gulp.src('app/*.html')
    .pipe(gulp.dest('dev'));
});

gulp.task('default', ['watch']);